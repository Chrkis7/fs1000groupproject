/*
************************************Global Variable Declaration******************************
*/

const gbtns = document.querySelectorAll('div.Category-btn button');
var gallery = document.querySelectorAll('div.gbox');

const stars = document.querySelector(".g-btn.starsbnt");
const planets = document.querySelector(".g-btn.planets");
const mysteries = document.querySelector(".g-btn.mysteries");
const heading = document.querySelector(".g-info-heading.stars-info")
const para = document.querySelector(".g-info-text.stars-text");

const galleryContainer = document.querySelector('div.gallery-sec');

const sizeOfgbox = 18;

/*
************************************Adding Event Handlers******************************
*/

stars.addEventListener('click', function() {
    heading.textContent = "STARS";
    para.textContent = "Space in Pictures: Stars";
})

planets.addEventListener('click', function() {
    heading.textContent = "PLANETS";
    para.textContent = "Space in Pictures: Planets";
})

mysteries.addEventListener('click', function() {
    heading.textContent = "MYSTERIES";
    para.textContent = "Space in Pictures: Mysteries";
})

window.addEventListener('load',galleryLoad())

gbtns.forEach(btn => {
    btn.addEventListener('click', categoryChange); 
 })

 gallery.forEach(gbox => {
    gbox.firstElementChild.addEventListener('click', enlargeImg);
})

/*
************************************Function Definitoins******************************
*/

 function galleryLoad(event){
    gallery.forEach(gbox => {
        const image = gbox.getElementsByTagName('img');
        image[0].style.display="none";
        image[0].parentElement.style.display ="none";
    })    
 }

 function categoryChange(event){
    //console.log( event.target.id );
    let largeImg = document.getElementById('largeImg');
    let imgId, imgSrc, divClass3; 

    if (typeof(largeImg) != 'undefined' && largeImg != null)
    {
        galleryContainer.removeChild(largeImg);

        for(let i = 0; i < sizeOfgbox; i++) {

            const newImg = document.createElement('img');
            const newgbox = document.createElement('div');

            if( i < 6)
            {
                imgId = 'm' + ((i % 6) + 1);
                imgSrc = 'images/photoMyst' + ((i % 6) + 1) + '.jpg';
                divClass3 = 'mystery';
            }
            else if (i > 5 && i < 12)
            {
                imgId = 's' + ((i % 6) + 1);
                imgSrc =  'images/photoStars' + ((i % 6) + 1) + '.jpg';;
                divClass3 = 'star';
            }
            else{
                imgId = 'p' + ((i % 6) + 1);
                imgSrc =  'images/photoPlanet' + ((i % 6) + 1) + '.jpg';;
                divClass3 = 'planet';
            }

            newImg.id = imgId;
            newImg.src = imgSrc;
            newImg.alt = "";
            newImg.width = "300";

            let divClass1 = 'g' + (i + 1);
            newgbox.classList.add(divClass1,"gbox",divClass3);

            newImg.style.display="none"
            newgbox.style.display="none";

            newgbox.appendChild(newImg);
            galleryContainer.appendChild(newgbox);
        }

        gallery = document.querySelectorAll('div.gbox');

        gallery.forEach(img => {
            img.firstElementChild.addEventListener('click', enlargeImg);
        })
    }

    gallery.forEach(gbox => {
        let image = gbox.getElementsByTagName('img');
        let src = image[0].src.toLowerCase();
        let str="photo";
        str=str.concat(event.target.id.slice(0,4));
        if(src.includes(str))
        {
            gbox.firstElementChild.style.display="block";
            gbox.style.display="block";
        }
        else
        {
            gbox.firstElementChild.style.display="none";
            gbox.style.display="none";
        }
    });
 }

function enlargeImg(event){
    let imgId = event.target.id;
    let imgSrc = event.target.src;

    while(galleryContainer.firstChild){
        galleryContainer.removeChild(galleryContainer.firstChild);
    }

    //Create englarged img and attach it
    let largeImg = document.createElement('img');
    galleryContainer.appendChild(largeImg);
    largeImg.src = imgSrc;
    largeImg.id = 'largeImg';
    largeImg.classList.add(imgId.charAt(0));

    largeImg.addEventListener('click', function(event) {

        let largeImg = document.getElementById('largeImg');
        let imgId, imgSrc, divClass3; 

        if (typeof(largeImg) != 'undefined' && largeImg != null)
        {
            galleryContainer.removeChild(largeImg);
    
            for(let i = 0; i < sizeOfgbox; i++) {
    
                const newImg = document.createElement('img');
                const newgbox = document.createElement('div');
    
                if( i < 6)
                {
                    imgId = 'm' + ((i % 6) + 1);
                    imgSrc = 'images/photoMyst' + ((i % 6) + 1) + '.jpg';
                    divClass3 = 'mystery';
                }
                else if (i > 5 && i < 12)
                {
                    imgId = 's' + ((i % 6) + 1);
                    imgSrc =  'images/photoStars' + ((i % 6) + 1) + '.jpg';;
                    divClass3 = 'star';
                }
                else{
                    imgId = 'p' + ((i % 6) + 1);
                    imgSrc =  'images/photoPlanet' + ((i % 6) + 1) + '.jpg';;
                    divClass3 = 'planet';
                }
    
                newImg.id = imgId;
                newImg.src = imgSrc;
                newImg.alt = "";
                newImg.width = "300";
    
                let divClass1 = 'g' + (i + 1);
                newgbox.classList.add(divClass1,"gbox",divClass3);
    
                newImg.style.display="none"
                newgbox.style.display="none";
    
                newgbox.appendChild(newImg);
                galleryContainer.appendChild(newgbox);
            }
    
            gallery = document.querySelectorAll('div.gbox');
    
            gallery.forEach(img => {
                img.firstElementChild.addEventListener('click', enlargeImg);
            })
        }

        let str = event.target.className;
        switch(str)
        {
            case 'm':
                str = 'photomyst';
                break;
            case 's':
                str = 'photostar';
                break;
            default:
                str = 'photoplan';
        }

        gallery.forEach(gbox => {
            let image = gbox.getElementsByTagName('img');
            let src = image[0].src.toLowerCase();

            if(src.includes(str))
            {
                gbox.firstElementChild.style.display="block";
                gbox.style.display="block";
            }
            else
            {
                gbox.firstElementChild.style.display="none";
                gbox.style.display="none";
            }
        });
    })
 }
